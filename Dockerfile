FROM microsoft/dotnet:2.0-sdk AS build
WORKDIR /app

# Copy csproj and restore as distinct layers
COPY *.csproj ./
RUN dotnet restore

# Copy everything else and build
COPY . ./
RUN dotnet publish -o out /p:PublishWithAspNetCoreTargetManifest="false"

# Build runtime image
FROM microsoft/dotnet:2.0-runtime AS runtime
ENV ASPNETCORE_URLS http://+:80
WORKDIR /app
COPY --from=build /app/out ./
ENTRYPOINT ["dotnet", "csharp-service.dll"]
